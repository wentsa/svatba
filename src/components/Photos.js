import React from "react";
import Gallery from "react-photo-gallery";
import Carousel, { Modal, ModalGateway } from "react-images";
import photosJson from '../_generated/photos';

class Photos extends React.Component {
    constructor(props) {
        super(props);

        this.state = {};

        Object.keys(photosJson).map((key) => this.state[key] = {
            currentImage: 0,
            viewerIsOpen: false,
        });
    }

    render() {
        return (
            <div>
                <div style={{
                    fontSize: '0.8rem',
                    fontFamily: 'Gravity',
                    fontStyle: 'italic',
                    marginBottom: '-2rem',
                    textAlign: 'right',
                }}>
                    <a href="/svatba-lea-a-tom.zip" download style={{ color: 'black' }}>Stáhnout vše</a>
                </div>
                {
                    Object.keys(photosJson)
                        .sort((a, b) => photosJson[a].order - photosJson[b].order)
                        .map((key) => {
                            const { name, photos } = photosJson[key];

                            const {
                                [key]: {
                                    currentImage,
                                    viewerIsOpen,
                                }
                            } = this.state;

                            return (
                                <div key={key}>
                                    <div
                                        style={{
                                            fontFamily: 'GreatVibes',
                                            fontSize: '3rem',
                                            textAlign: 'left',
                                            width: '100%',
                                            marginBottom: '0.2rem'
                                        }}
                                    >{name}</div>
                                    <div style={{ marginBottom: '3rem' }}>
                                        <Gallery
                                            photos={photos}
                                            onClick={(event, { index }) => {
                                                this.setState({
                                                    [key]: {
                                                        currentImage: index,
                                                        viewerIsOpen: true,
                                                    }
                                                })
                                            }}
                                        />
                                    </div>
                                    <ModalGateway>
                                        {viewerIsOpen ? (
                                            <Modal onClose={() => {
                                                this.setState({
                                                    [key]: {
                                                        viewerIsOpen: false,
                                                    }
                                                });
                                            }}>
                                                <Carousel
                                                    currentIndex={currentImage}
                                                    views={photos.map(x => ({
                                                        ...x,
                                                        srcset: x.srcSet,
                                                        caption: x.title
                                                    }))}
                                                />
                                            </Modal>
                                        ) : null}
                                    </ModalGateway>
                                </div>
                            );
                        })
                }
            </div>
        );
    }
}

export default Photos;
