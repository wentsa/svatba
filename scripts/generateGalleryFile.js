const GALLERY_DIR = '../public/gallery';
const ZIP_LOCATION = '/../public/svatba-lea-a-tom.zip';
const config = require('./config');

const fs = require('fs');
const archiver = require('archiver');
const sizeOf = require('image-size');

const readFiles = (dirname, onFileContent, onError, onComplete) => {
    fs.readdir(dirname, (err, filenames) => {
        if (err) {
            onError(err);
            return;
        }

        let done = 0;

        filenames.forEach((filename) => {
            fs.readFile(dirname + '/' + filename, 'utf-8', (err, content) => {
                if (err) {
                    onError(err);
                    return;
                }
                onFileContent(filename, () => {
                    done++;
                    if (done === filenames.length) {
                        onComplete();
                    }
                });
            });
        });
    });
};

const createZip = (input) => {
    const output = fs.createWriteStream(__dirname + ZIP_LOCATION);
    const archive = archiver('zip', {
        zlib: { level: 9 } // Sets the compression level.
    });

    // listen for all archive data to be written
    // 'close' event is fired only when a file descriptor is involved
    output.on('close', function() {
        console.log(archive.pointer() + ' total bytes');
        console.log('archiver has been finalized and the output file descriptor has closed.');
    });

    // This event is fired when the data source is drained no matter what was the data source.
    // It is not part of this library but rather from the NodeJS Stream API.
    // @see: https://nodejs.org/api/stream.html#stream_event_end
    output.on('end', function() {
        console.log('Data has been drained');
    });
    // good practice to catch warnings (ie stat failures and other non-blocking errors)
    archive.on('warning', function(err) {
        if (err.code === 'ENOENT') {
            // log warning
        } else {
            // throw error
            throw err;
        }
    });

    // good practice to catch this error explicitly
    archive.on('error', function(err) {
        throw err;
    });

    // pipe archive data to the file
    archive.pipe(output);

    // append files from a sub-directory and naming it `new-subdir` within the archive
    input.map(({ dir, name }) => {
        archive.directory(dir, name);
    });

    // finalize the archive (ie we are done appending files but streams have to finish yet)
    // 'close', 'end' or 'finish' may be fired right after calling this method so register to them beforehand
    archive.finalize();
};

const galleryInput = {};
const zipInput = [];

let completedDirs = 0;
for (let i = 0; i < config.length; i++) {
    const {
        dir,
        name,
        order,
    } = config[i];

    const subdir = GALLERY_DIR + '/' + dir;

    zipInput.push({ dir: subdir, name });

    const photos = [];
    readFiles(subdir,
        (filename, onComplete) => {
            sizeOf(subdir + '/' + filename, (err, dimensions) => {
                photos.push({
                   src: `/gallery/${dir}/${filename}`,
                   width: dimensions.width,
                   height: dimensions.height,
                });
                onComplete();
            });
        },
        (err) => console.error(err),
        () => {
            galleryInput[dir] = {
                name,
                order,
                photos: photos.sort((a, b) => a.src.localeCompare(b.src, 'en', { numeric: true })),
            };

            completedDirs++;
            if (completedDirs === config.length) {
                console.log(zipInput);

                fs.writeFileSync('../src/_generated/photos.json', JSON.stringify(galleryInput, null, 4) , 'utf-8');

                createZip(zipInput);
            }
        }
    );
}

